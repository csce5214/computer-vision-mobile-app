from flask import Flask, request, jsonify
from flask_restful import Resource, Api, reqparse, abort
import json
import os
import sys
import requests
import time
# If you are using a Jupyter notebook, uncomment the following line.
# %matplotlib inline
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon
from PIL import Image
from io import BytesIO

app = Flask(__name__)
api = Api(app)

missing_env = False


# Add your Computer Vision subscription key and endpoint to your environment variables.
# export COMPUTER_VISION_ENDPOINT="https://projectworkspace.cognitiveservices.azure.com/"
# export COMPUTER_VISION_SUBSCRIPTION_KEY="b3bdcc6782554d6ba887637dcdf0c733"
if 'COMPUTER_VISION_ENDPOINT' in os.environ: 
    endpoint = os.environ['COMPUTER_VISION_ENDPOINT']
else:
    print("From Azure Cognitive Service, retrieve your endpoint and subscription key.")
    print("\nSet the COMPUTER_VISION_ENDPOINT environment variable, such as \"https://westus2.api.cognitive.microsoft.com\".\n")
    missing_env = True

if 'COMPUTER_VISION_SUBSCRIPTION_KEY' in os.environ: 
    subscription_key = os.environ['COMPUTER_VISION_SUBSCRIPTION_KEY']
else:
    print("From Azure Cognitive Service, retrieve your endpoint and subscription key.")
    print("\nSet the COMPUTER_VISION_SUBSCRIPTION_KEY environment variable, such as \"b3bdcc6782554d6ba887637dcdf0c733\".\n")
    missing_env = True

if missing_env:
    print("**Restart your shell or IDE for changes to take effect.**")
    sys.exit()

text_recognition_url = endpoint + "/vision/v3.0/read/analyze"

def readImage():
    headers = {'Ocp-Apim-Subscription-Key': subscription_key}
    data = {'url': os.image_url}
    response = requests.post(
        text_recognition_url, headers=headers, json=data)
    response.raise_for_status()

    # Extracting text requires two API calls: One call to submit the
    # image for processing, the other to retrieve the text found in the image.

    # Holds the URI used to retrieve the recognized text.
    os.operation_url = response.headers["Operation-Location"]

    # The recognized text isn't immediately available, so poll to wait for completion.
    analysis = {}
    poll = True
    while (poll):
        response_final = requests.get(
            response.headers["Operation-Location"], headers=headers)
        analysis = response_final.json()
        os.imageAPI = analysis
        time.sleep(1)
        if ("analyzeResult" in analysis):
            poll = False
        if ("status" in analysis and analysis['status'] == 'failed'):
            poll = False

    os.polygons = []
    if ("analyzeResult" in analysis):
        # Extract the recognized text, with bounding boxes.
        os.polygons = [(line["boundingBox"], line["text"])
                    for line in analysis["analyzeResult"]["readResults"][0]["lines"]]

parser = reqparse.RequestParser()
parser.add_argument('image')

class Images(Resource):
    def get(self):
        return jsonify(os.imageAPI)

    def post(self):
        parser.add_argument('url')
        args = parser.parse_args()
        os.image_url = args.url
        readImage()
        pass


app.route('/')


def index():
    return "Simple API with Restful"


# api.add_resource(Images, '/api/images/<string:title>')

api.add_resource(Images, '/api/image')

if __name__ == '__main__':
    app.run(debug=True)
