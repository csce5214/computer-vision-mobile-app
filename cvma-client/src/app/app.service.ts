import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from  '@angular/common/http';  
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
/**
 * Handle REST API here
 */
@Injectable({
  providedIn: 'root'
})
export class AppService {
  /**
   * HttpClient is a service with methods to perform HTTP requests
   * @link https://angular.io/api/common/http/HttpClient|HttpClient
   * @param http HttpClient
   */
  constructor(private http: HttpClient) {}

  /**
   * Send the image url to app.py through POST method
   * @param url string
   */
  setFile(url) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.http.post(`/api/image`, url, httpOptions);
  }

  /**
   * Get the text from images through REST API from app.py
   */
  getFile(): Observable<any> {
    return this.http.get(`/api/image`).pipe(
      map((response: any) => {
        return response.analyzeResult.readResults[0].lines.map(line => line.text);
      })
    );
  }
}
