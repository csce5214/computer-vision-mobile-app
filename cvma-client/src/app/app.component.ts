import { Component } from '@angular/core';

/**
 * Parent Component
 * 
 * Display Image, Input url and output text is handled from this component html
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  /**
   * hold image url to display in img src in template
   */
  imgSrc: string;

  /**
   * Image url will be reassigned as the submit button is clicked through this function
   * @param image string
   */
  updateImage(image) {
    this.imgSrc = image.url
  }
}
