/**
 * @author Sanjib Paudel
 */
import { Component } from '@angular/core';
import { AppService } from '../app.service';

/**
 * Display the text/output by requesting the json through GET API from app.py
 */
@Component({
  selector: 'app-output',
  templateUrl: './output.component.html'
})
export class OutputComponent {
  /**
   * Array of  string that will be used in template with loop
   */
  outputTexts: string[];

  /**
   * Define global variables for this component
   * 
   * Add all services in the constructor to have it accessible in this component.
   * 
   * Use private if you want it to be only available in .ts file and public if you want it to be available in template as well.
   * @param appService Appservice
   */
  constructor(private appService: AppService) { }

  /**
   * This function will be triggered from select component.
   * 
   * The function will request the text from REST API get method from app.py and assign the value to outputTexts.
   */
  getTextFromImage() {
    return this.appService.getFile().subscribe(response => {
      this.outputTexts = response;
    });
  }

}
