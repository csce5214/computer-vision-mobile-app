import { Component, EventEmitter, Output } from '@angular/core';
import { AppService } from '../app.service';
import { NgForm } from '@angular/forms';

/**
 * Display the input box to type the image url and submit button to pass the url to app.py through POST method
 */
@Component({
  selector: 'app-select',
  templateUrl: './select.component.html'
})
export class SelectComponent {
  /**
   * pass the value to parent component
   */
  @Output() selected = new EventEmitter<string>();
  /**
   * Display loader if Loading is true
   */
  isLoading: boolean;

  /**
   * Display error message when GET response failss
   */
  message: string;

  /**
   * Define global variables for this component
   * 
   * Add all services in the constructor to have it accessible in this component.
   * 
   * Use private if you want it to be only available in .ts file and public if you want it to be available in template as well. 
   * @param appService AppService
   */
  constructor(private appService: AppService) { }

  /**
   * On submit, the url will be submitted to app.py through REST API POST method.
   * 
   * The function is passing the url value to appService and waiting for the response in subscribe method.
   * 
   * Once response is received, it will pass the value to output through emit
   * @param f NgForm
   */
  onSubmit(f: NgForm) {
    this.isLoading = true;
    this.message = null;
    this.appService.setFile(f.value).subscribe(() => {
      this.selected.emit(f.value);
      this.isLoading = false;
    });
  }

}
