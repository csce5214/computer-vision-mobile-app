# CvmaClient

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.0.

## Developers
- Sanjib Paudel
- Olusola Amusan
- Tanakit Phamornratanakun

## Pre-requisites
- Install NodeJS 12.18.x and above (https://nodejs.org/en/)
- angular cli by running `npm install -g @angular/cli`
- Install node package modules by running `npm install`

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `npm run build:prod` for a production build.

## Documentation
- Run `npm install -g @compodoc/compodoc` for one time install
- Run `npm install` for one time install if incase compodoc is not installed.
- Run `npm run compodoc` to generate the documentation
- Run `compodoc -s` to start the server for documentation and launch frontend [documentation](http://localhost:8080/)(http://localhost:8080/)

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
