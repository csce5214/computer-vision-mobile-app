# Computer Vision Mobile App
**Mobile app for Reading text and handwriting from a document/image** such as a medical hand-writing using Azure computer vision api and python (details in project list)

### Project Abstract:

Converting doctor notes and prescriptions into digital form has been problematic for many years. According to UFS Health, Electronic Health Records (EHR) provide significant benefits to physicians, patients and healthcare systems. Although the new generation of healthcare industry has digitized health records, there are a lot more documents that need to be converted with limitations of technology accessibility.

Intended to solve this problem of the healthcare industry, this project is aiming for giving a convenient, accessible and reliable mobile application that can easily convert physical patient health records into digital form. Any hand-written documents can be converted by uploading through the application. The picture of documents will be processed by Azure’s Computer Vision API, then the response of digital text will be sent back within a few seconds.

In conclusion, the hand-written document reader is the open-door to many great possibilities for the healthcare industry. Mobile applications with computer vision can be used for anomalies detection or even primary medical diagnosis. Our group hopes that this project will benefit the medical and AI industry in the future.

### Technical Requirement:
- Azure - Computer Vision API
	- Convert uploaded image into digitized form.
- Python
- SQL
- Angular: [Readme](cvma-client/README.md)
	- Create a web app / mobile app for user interface to upload images/take images for handwritten text to digital font. 
- Bootstrap:
	- Create a responsive web design to make it user friendly for all kinds of devices.
- REST: 
	- We will use REST API to perform the CRUD operation between backend and frontend.
- REPOSITORY and VERSION CONTROL:
	- Bitbucket

